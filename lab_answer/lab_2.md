1. Apakah perbedaan antara JSON dan XML?
XML (Extensible Markup Language)
XML adalah bahasa markup yang dirancang untuk menyimpan data dan transfer data. 
- Bahasa : bahasa markup, bukan bahasa pemprograman, memiliki tag untuk mendefinisikan suatu elemen
- Penyimpanan data : data disimpan sebagai tree structure
- Pengolahan : dapat melakukan pemrosesan dan pemformatan dokumen dan objek
- Kecepatan : besar dan lambat dalam penguraian, sehingga menyebabkan transmisi data lebih lambat
- Dukungan Namespaces : mendukung namespaces, komentar dan metadata
- Ukuran dokumen : ukuran dokumen besar dan file besar, struktur tag membuatnya besar dan susah untuk dibaca.
- Dukungan Array : tidak mendukung array secara langsung
- Jenis Data Dukungan : mendukung banyak tipe data kompleks termasuk bagan, charts, dan tipe data non-primitif lainnya.
- Dukungan UTF : mendukung pengkodean UTF-8 dan UTF-16.

Kelebihan XML : 
- Dengan bantuan XML, pertukaran data dapat dikukan dengan cepat antara platform yang berbeda. Jadi dokumen dapat dipindahkan ke seluruh sistem dan aplikasi
- XML memisahkan data dari HTML
- XML menyederhanakan proses perubahan platform 

Kekurangan XML :
- XML membutuhkan aplikasi pemrosesan.
- Sintaks XML terkadang bisa membingungkan karena mirip dengan alternatif lain. 
- Tidak ada dukungan tipe data intrinsik.
- Sintaks XML berlebihan.
- tidak memungkinkan pengguna untuk membuat tag-nya.


JSON (JavaScript Object Notation)
JSON digunakan untuk menyimpan informasi dengan cara yang terorganisir dan mudah diakses. Kumpulan data yang dapat dibaca manusia yang dapat diakses secara logis.
- Bahasa : hanya suatu format yang ditulis dalam JavaScript
- Penyimpanan data : data disimpan seperti map/dic dengan pasangan key dan value
- Pengolahan : tidak dapat melakukan pemprosesan atau perhitungan apapun
- Kecepatan : sangat cepat karena ukuran file sangat kecil, penguraian lebih cepat oleh mesin JavaScript, sehingga trasfer data lebih cepat
- Dukungan Namespaces : tidak ada ketentuan untuk namespace, menambahkan komentar atau menulis metadat
- Ukuran dokumen : ringkas dan mudah dibaca, tidak ada tag atau data yang digunakan, membuat file sederhana.
- Dukungan Array : mendukung array yang dapat diakses
- Jenis Data Dukungan : hanya mendukung string, angka, array Boolean, dan objek. Bahkan objek hanya dapat berisi tipe primitif.
- Dukungan UTF : mendukung penyandiaksaraan UTF serta ASCII.

Kelebihan JSON :
- Mendukung semua browser. 
- Mudah dipahami.
- Sintaksnya sangat mudah. 
- Dapat diurai dalam JavaScript menggunakan fungsi eval()
- Creation dan manipulation itu mudah. 
- Semua JavaScript frameworks utama menawarkan dukungan JSON.
- Sebagian besar teknologi backend mendukung JSON. 
- Transmisi dan serialisasi data terstruktur dilakukan menggunakan koneksi jaringan.

Kekurangan JSON :
- JSON menawarkan ekstensibilitas yang buruk karena tidak ada dukungan namespace.
- Dukungan tool pengembangan terbatas.
- Memberikan dukungan untuk definisi tata bahasa formal


-> Intinya XML dan JSON adalah cara kita untuk mengatur data dalam bentuk format yag dapat dimengerti oleh banyaj bahasa pemrograman dan API. Untuk pertukaran data sederhana yang tidak memerlukan sematik dan validasi, JSON cocok untuk digunakan. Sedangkan jika kita memerlukan untuk melakukan lebih dari sekedar pertukaran data dan pemrosesan cepat, pilih xml. 

2. Apakah perbedaan antara HTML dan XML?
XML (Extensible Markup Language)
- XML adalah singkatan dari eXtensible Markup Language sedangkan HTML adalah singkatan dari Hypertext Markup Language.
- XML berfokus pada transfer data sedangkan HTML difokuskan pada penyajian data.
- XML didorong konten sedangkan HTML didorong oleh format.
- XML itu Case Sensitive sedangkan XML Case Insensitive
- XML menyediakan dukungan namespaces sementara HTML tidak menyediakan dukungan namespaces.
- XML strict untuk tag penutup sedangkan HTML tidak strict.
- Tag XML dapat dikembangkan sedangkan HTML memiliki tag terbatas.
- Tag XML tidak ditentukan sebelumnya sedangkan HTML memiliki tag yang telah ditentukan sebelumnya.

Kelebihan HTML :
- Browser dokumen HTML mudah dibuat.
- HTML bekerja untuk sistem yang tidak terkait juga.
- HTML mudah dimengerti dan memiliki sintaks yang sangat sederhana.
- Dapat menggunakan tag yang berbeda untuk membuat halaman web Anda.
- Memungkinkan untuk menggunakan berbagai warna, objek, dan tata letak

Kekurangan HTML :
- HTML tidak memiliki pemeriksaan sintaks dan struktur
- HTML tidak cocok untuk pertukaran data
- HTML tidak mengizinkan untuk mendeskripsikan konten informasi atau semantik dokumen
- HTML tidak berorientasi objek, jadi HTML bukan bahasa yang dapat dikembangkan dan sangat tidak stabil
- Penyimpanan data dan pertukaran data tidak dimungkinkan menggunakan HTML

Kelebihan XML : 
- Dengan bantuan XML, pertukaran data dapat dikukan dengan cepat antara platform yang berbeda. Jadi dokumen dapat dipindahkan ke seluruh sistem dan aplikasi
- XML memisahkan data dari HTML
- XML menyederhanakan proses perubahan platform 

Kekurangan XML :
- XML membutuhkan aplikasi pemrosesan.
- Sintaks XML terkadang bisa membingungkan karena mirip dengan alternatif lain. 
- Tidak ada dukungan tipe data intrinsik.
- Sintaks XML berlebihan.
- tidak memungkinkan pengguna untuk membuat tag-nya.

-> Intinya HTML membantu untuk mengembangkan suatu struktur halaman web, sedangkan XML membantu untuk bertukar data antar platform yang berbeda