from django.db import models

# Create your models here.
# define variabel yang kita mau
class Note(models.Model):
    #to
    to = models.CharField(max_length=50)

    #from
    fromF = models.CharField(max_length=50)

    #title
    title = models.CharField(max_length=50)

    #message
    message = models.TextField()