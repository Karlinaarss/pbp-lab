from django.shortcuts import render
from django.http.response import HttpResponse
from django.core import serializers

# Create your views here.
from lab_2.models import Note



def index(request):
    # ambil data yang ada di Note
    # dalam bentuk set
    notes = Note.objects.all().values()
    
    # ngirim dalam bentuk dic 
    response = {'catatan': notes}

    #request parameter pasti
    # yang tengah tujuan pengiriman
    # respone : apa yg pengen dikirim
    return render(request, 'lab2.html', response)

def xml(request):
    notes = Note.objects.all().values()
    data = serializers.serialize('xml', Note.objects.all())
    return HttpResponse(data, content_type="application/xml")

def json(request):
    notes = Note.objects.all().values()
    data = serializers.serialize('json', Note.objects.all())
    return HttpResponse(data, content_type="application/json")
