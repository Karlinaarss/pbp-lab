from django.urls import path
from .views import add_note, index, note_list


urlpatterns = [
    path('', index, name='index'),
    path('addNote', add_note, name='add_note'),
    path('note', note_list, name='note_list'),
]