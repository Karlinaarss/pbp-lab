from django.shortcuts import render
from django.http.response import HttpResponseRedirect
from lab_2.models import Note
from .forms import NoteForm
from django.contrib.auth.decorators import login_required

# Create your views here.
# @login_required(login_url='/admin/login/')
def index(request):
    # ambil data yang ada di Note
    # dalam bentuk set
    notes = Note.objects.all().values()
    
    # ngirim dalam bentuk dic 
    response = {'catatan': notes}

    #request parameter pasti
    # yang tengah tujuan pengiriman
    # respone : apa yg pengen dikirim
    return render(request, 'lab4_index.html', response)

# @login_required(login_url='/admin/login/')
def add_note(request):
    response = {}
    note = NoteForm(request.POST or None)
    if(note.is_valid() and request.method == 'POST'):  
        note.save()
        return HttpResponseRedirect('/lab-4/note')
    response['note'] = note
    return render(request, 'lab4_form.html', response)

# @login_required(login_url='/admin/login/')
def note_list(request):
    notes = Note.objects.all().values()
    response = {'catatan': notes}
    return render(request, 'lab4_note_list.html', response)
