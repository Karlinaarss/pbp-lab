from django.db import models

class Friend(models.Model):
    #nama
    name = models.CharField(max_length=30)

    #npm
    npm = models.CharField(max_length=10)

    #format 2002-1-10
    dob = models.DateField()
