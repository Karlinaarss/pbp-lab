from django.shortcuts import render
from django.http.response import HttpResponseRedirect
from lab_1.models import Friend
from .forms import FriendForm
from django.contrib.auth.decorators import login_required

@login_required(login_url='/admin/login/')
def index(request):
    friends = Friend.objects.all()
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)

@login_required(login_url='/admin/login/')
def add_friend(request):
    response = {}
    asal = FriendForm(request.POST or None)
    if(asal.is_valid() and request.method == 'POST'):
        asal.save()
        return HttpResponseRedirect('/lab-3')
    response['asal'] = asal
    return render(request, 'lab3_form.html', response)


